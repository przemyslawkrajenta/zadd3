---
author: Przemysław Krajenta
title: Krzysztof Krawczyk
subtitle: 
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

# Życiorys Krzysztofa Krawczyka

\textcolor{red}{Krzysztof Krawczyk – polski piosenkarz, urodził się 8 września 1946 r. w Katowicach. Rodzina Krawczyków często zmieniała miejsca zamieszkania ze względu na charakter pracy rodziców Krzysztofa – barytona i śpiewaczki operowej. Przyszły piosenkarz uczył się śpiewu w szkołach muzycznych, po śmierci ojca musiał jednak przerwać edukację. Jego matka wpadła w depresję, a on stał się żywicielem rodziny. Krzysztof Krawczyk debiutował w 1963 r. razem z zespołem Trubadurzy. Grupa nagrała takie przeboje, jak: „Znamy się tylko z widzenia”, „Krajobrazy”, „Byłaś tu”, „Ej, Sobótka, Sobótka”. Swoją popularnością dorównywali Czerwonym Gitarom. }


# Kariera

\textcolor{blue}{Karierę solową Krzysztof Krawczyk rozpoczął w 1973 r. płytą „Byłaś mi nadzieją” i programem „Kochałem Panią”. W 1976 r. na Festiwalu w Sopocie otrzymał 3 nagrody: Dziennikarzy, Publiczności i Głosu Wybrzeża. Rok później Krawczyk wziął udział w spektaklu „Staroświecka komedia” Arbuzowa, w którym śpiewał wplecione w fabułę poezje Jesienina. Wkrótce potem nagrał swoje słynne utwory „Jak minął dzień” i „Pamiętam ciebie z tamtych lat”. W 1980 r. Krzysztof Krawczyk wyjechał na serię koncertów do Stanów Zjednoczonych i postanowił zostać za oceanem. Tam występował w ekskluzywnych klubach, zwłaszcza w Las Vegas. Po powrocie do Polski w 1985 r. zdobył II nagrodę na Festiwalu w Opolu za piosenkę „Nie przesadza się starych drzew”}. 

#

\begin{center}{\textcolor{green}{Kilka lat później Krawczyk uległ poważnemu wypadkowi samochodowemu i musiał poddać się rehabilitacji. Ponownie wyjechał do Stanów, gdzie przechodził dalszą rekonwalescencję i nagrywał kolejne płyty. W 1994 r., już w Polsce, nagrał płytę „Gdy nam śpiewał Elvis Presley”. W 2000 r. Krzysztof Krawczyk zaśpiewał przed papieżem. Razem z Goranem Bregoviĉem stworzył album „Daj mi drugie życie” w 2001 r. Podczas Festiwalu Top Trendy w Sopocie świętował 45-lecie pracy artystycznej.}}
\end{center}


# Największe utwory Krzysztofa Krawczyka


* Byle Było Tak. Krzysztof Krawczyk.
* To Co Dał Nam świat. Krzysztof Krawczyk.
* Za Tobą Pójdę Jak Na Bal. Krzysztof Krawczyk.
* Ostatni Raz Zatańczysz Ze Mną Krzysztof Krawczyk.
* Mój Przyjacielu. ...
* Bo Jesteś Ty. ...
* Trudno Tak (razem Być Nam Ze Sobą...) ...
* Tylko Ty, Tylko Ja.


# Dyskografia Krzysztofa Krawczyka 

###
Albumy studyjne - 40

###
Albumy koncertowe - 4

###
Kompilacje - 33

###
Single - 14

###
Teledyski - 40

###
Ścieżki dźwiękowe, utwory w filmach - 13


#  Najpopularniejsze Albumy Krzysztofa Krawczyka

| Album                 | Rok            | Ilość utworów          |
| -------------         |:-------------: | -----:        |
| The Very Best of      | 2011           | 16         |
| Duety           | 2016       |   12         |
| A kiedy będziesz moją żoną        | 2011      |   14      |
| Concert Hits       | 2019      |   18      |
| ...bo marzę i śnię       | 2002    |   12      |
| My cyganie       | 2004      |   12      |
| To Co W Życiu Ważne       | 2004      |   13      |


#
![](C:\Users\Przemek\zad3\zad3/al1.jpg){ height=100% width=100%}

#
![](C:\Users\Przemek\zad3\zad3/al2.jpg){ height=100% width=100%}

#
![](C:\Users\Przemek\zad3\zad3/al3.jpg){ height=100% width=100%}

#
![](C:\Users\Przemek\zad3\zad3/al4.jpg){ height=100% width=100%}